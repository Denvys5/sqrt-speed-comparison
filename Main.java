/*
 * 1/SQRT Benchmark
 * Rules:
 * 1) Code calculates inverse square root as 1/sqrt(x)
 * 2) Calculation is repeated with x changing by 0.001 each iteration
 * 3) All code is contained within single method, count begins on its execution and stops after its completion
 * 4) Only executable code, no code skipping
 * 5) All variables, required for the calculation, must be initialized within this method
 * 6) To avoid dead code elimination, count sum of results on each iteration
 * 7) Adequate accuracy, at least 4 digits
 * 8) BENCHMARK_SIZE = 10,000,000 (any more may cause problems)
 * 9) set your current CPU_CLOCKSPEED to measure clock-time
 *
 */


import java.time.Duration;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class Main {
    private static final int BENCHMARK_SIZE = 10000000;
    private static final double clockspeed = 4.4e+9;
    public void testTime(String name, Supplier<Float> function){
        long stop;
        long start = System.nanoTime();
        function.get();
        stop = System.nanoTime();
        long difference = stop-start;
        printResult(name, difference);
    }

    public void warmUpTest(String name, int warmup_size, Supplier<Float> function){
        for (int i = 0; i < warmup_size; i++)
            function.get();
        long stop;
        long start = System.nanoTime();
        function.get();
        stop = System.nanoTime();
        long difference = stop-start;
        printResult(name, difference);
    }

    public void printResult(String name, long difference){
        Duration duration = Duration.ofNanos(difference);
        String stringDifference = String.format("%s.%s ms", duration.toMillisPart(), duration.toNanosPart()/1000%1000);
        long clocks = Math.round(((double) difference)/((long)BENCHMARK_SIZE)*clockspeed/1000000000);
        System.out.println(name + ": " + stringDifference + " (" + clocks + " clock-cycles)");
    }

    public float normalBenchmark(){
        float y = 0;
        float x = 1.0F;
        float result = 0;
        for( int i = 0; i < BENCHMARK_SIZE; ++i ) {
            y = (float) (1.0f / Math.sqrt(x));
            x += 0.001f;

            result+=y; //Added this to prevent dead code elimination
        }
        return result;
    }

    public float fastBenchmark(){
        float y = 0;
        float x = 1.0F;
        float result = 0;
        for( int i = 0; i < BENCHMARK_SIZE; ++i ) {
            y = (float) (1.0f / fastSqrt(x));
            x += 0.001f;

            result+=y; //Added this to prevent dead code elimination
        }
        return result;
    }

    public float quakeSqrtBenchmark(){
        float y = 0;
        float x = 1.0F;
        float result = 0;
        for( int i = 0; i < BENCHMARK_SIZE; ++i ) {
            y = Q_rsqrt(x);
            x += 0.001f;

            result+=y; //Added this to prevent dead code elimination
        }
        return result;
    }

    public float testInsertedBenchmark(UnaryOperator<Float> equation){
        float y = 0;
        float x = 1.0F;
        float result = 0;
        for( int i = 0; i < BENCHMARK_SIZE; ++i ) {
            y = equation.apply(x);
            x += 0.001f;

            result+=y; //Added this to prevent dead code elimination
        }
        return result;
    }


    public float skipBenchmark(){
        float y = 0;
        float x = 1.0F;
        for( int i = 0; i < BENCHMARK_SIZE; ++i ) {
            y = (float) (1.0f / Math.sqrt(x));
            x += 0.001f;
        }
        return y;
    }

    public double fastSqrt(double input){
        double sqrt = Double.longBitsToDouble( ( ( Double.doubleToLongBits( input )-(1L <<52) )>>1 ) + (1L <<61 ) );
        return (sqrt + input/sqrt)/2.0;
    }

    public float Q_rsqrt(float number){
        int i;
        float x2, y;
	    final float threehalfs = 1.5F;

        x2 = number * 0.5F;
        y  = number;
        i  = Float.floatToRawIntBits(y);                       // evil floating point bit level hacking
        i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
        y  = Float.intBitsToFloat(i);
        y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
    	y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

        return y;
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.testTime( "Normal        ", main::normalBenchmark);
        main.testTime( "Fast          ", main::fastBenchmark);
        main.testTime( "Skip          ", main::skipBenchmark);
        main.testTime( "Quake         ", main::quakeSqrtBenchmark);
        main.testTime( "Inserted      ", ()-> main.testInsertedBenchmark(main::Q_rsqrt));
        System.out.println();
        main.warmUpTest( "Warmup Normal ", 5, main::normalBenchmark);
        main.warmUpTest( "Warmup Fast   ", 5, main::fastBenchmark);
        main.warmUpTest( "Warmup Skip   ", 5, main::skipBenchmark);
        main.warmUpTest( "Warmup Quake  ", 5, main::quakeSqrtBenchmark);
        main.warmUpTest( "Warmup Inserted", 5, ()-> main.testInsertedBenchmark(main::Q_rsqrt));
    }
}
