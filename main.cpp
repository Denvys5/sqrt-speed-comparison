/*
 * 1/SQRT Benchmark
 * Rules:
 * 1) Code calculates inverse square root as 1/sqrt(x)
 * 2) Calculation is repeated with x changing by 0.001 each iteration
 * 3) All code is contained within single method, count begins on its execution and stops after its completion
 * 4) Only executable code, no code skipping
 * 5) All variables, required for the calculation, must be initialized within this method
 * 6) To avoid dead code elimination, count sum of results on each iteration
 * 7) Adequate accuracy, at least 4 digits
 * 8) BENCHMARK_SIZE = 10,000,000 (any more may cause problems)
 * 9) set your current CPU_CLOCKSPEED to measure clock-time
 *
 */

#include <iostream>
#include <chrono>
#include <cmath>

enum { BENCHMARK_SIZE = 10000000 };
#define clockspeed 4.4e+9

using Func = float (*)();

void testTime( const char* name, Func func )
{
    auto start = std::chrono::high_resolution_clock::now();
    func();
    auto end   = std::chrono::high_resolution_clock::now();
    auto diff = end - start;
    auto dur = std::chrono::duration <double, std::milli> (diff).count();
    auto clocks = round(std::chrono::duration <double, std::milli> (diff).count()/((long)BENCHMARK_SIZE)*clockspeed/1000);
    std::cout << name << ": " << dur << " ms (" << clocks << " clock-cycles)" << std::endl;
}

void testTimeWithWarmup( const char* name, int warmup_size, Func func )
{
    for(int i = 0; i < warmup_size; i++)
        func();
    auto start = std::chrono::high_resolution_clock::now();
    func();
    auto end   = std::chrono::high_resolution_clock::now();
    auto diff = end - start;
    auto dur = std::chrono::duration <double, std::milli> (diff).count();
    auto clocks = round(std::chrono::duration <double, std::milli> (diff).count()/((long)BENCHMARK_SIZE)*clockspeed/1000);
    std::cout << name << ": " << dur << " ms (" << clocks << " clock-cycles)" << std::endl;
}


//https://github.com/id-Software/Quake-III-Arena/blob/dbe4ddb10315479fc00086f08e25d968b4b43c49/code/game/q_math.c#L552
inline float Q_rsqrt( float number )
{
    long i;
    float x2, y;
    const float threehalfs = 1.5F;

    x2 = number * 0.5F;
    y  = number;
    i  = * ( long * ) &y;						// evil floating point bit level hacking
    i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
    y  = * ( float * ) &i;
    y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
//	y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

    return y;
}

float normalBenchmark()
{
    float y = 0;
    float x = 1.0;
    for( int i = 0; i < BENCHMARK_SIZE; ++i ) {
        y = 1.0f / sqrt(x);
        x += 0.001f;
    }
    return y;
}

float stdBenchmark()
{
    float y = 0;
    float x = 1.0;
    for( int i = 0; i < BENCHMARK_SIZE; ++i ) {
        y = 1.0f / std::sqrt(x);
        x += 0.001f;
    }
    return y;
}

float sqrtfBenchmark()
{
    float y = 0;
    float x = 1.0;
    for( int i = 0; i < BENCHMARK_SIZE; ++i ) {
        y = 1.0f / sqrtf(x);
        x += 0.001f;
    }
    return y;
}

float fastBenchmark()
{
    float y = 0;
    float x = 1.0;
    for( int i = 0; i < BENCHMARK_SIZE; ++i ) {
        y = Q_rsqrt(x);
        x += 0.001f;
    }
    return y;
}

float sseRSqrt(float x )
{
    float y;

    asm
    (
            "rsqrtss %[x], %%xmm0;"   // EVAL rsqrtss of "x" and store result in xmm0
            "movss %%xmm0, %[y];"     // LOAD value from xmm0 into y
        :
        : [ x ] "m" ( x ),
          [ y ] "m" ( y )
    #ifdef __X86_64__
        : "%xmm0"
    #endif
    );

    return y;
}

float sseBenchmark()
{
    float y = 0;
    float x = 1.0;
    for (int i = 0; i < BENCHMARK_SIZE; ++i) {
        y = sseRSqrt(x);
        x += 0.001f;
    }
    return y;
}

int main()
{
    testTime( "Normal   ", normalBenchmark );
    testTime( "Std      ", stdBenchmark );
    testTime("sqrtf    ", sqrtfBenchmark);
    testTime( "Fast     ", fastBenchmark );
    testTime( "SSE      ", sseBenchmark );
    std::cout << std::endl;
    testTimeWithWarmup( "Normal   ", 5, normalBenchmark );
    testTimeWithWarmup( "Std      ", 5, stdBenchmark );
    testTimeWithWarmup("sqrtf    ", 5, sqrtfBenchmark);
    testTimeWithWarmup( "Fast     ", 5, fastBenchmark );
    testTimeWithWarmup( "SSE      ", 5, sseBenchmark );
    return 0;
}